<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::post('/validate', 'LoginController@validateUser');

Route::post('/Auth2validate','LoginController@Auth2validate');

Route::get('/error_404/{heading}/{message}',function($heading,$message) {

    return view('errors.error_404', compact('heading','message'));

});


//Microsoft Authentication Manual work
Route::get('ms_authorize', 'LoginController@getToken');


Route::get('/privacy-policy', 'AppController@getPrivacy');

Route::middleware('auth2')->group(function() {

    Route::post('/logout', 'LoginController@logout');
    Route::get('/Engineer_route', 'EngineerController@EngineerIndex');
    Route::get('/eng/listing', 'EngineerController@listing');

    Route::get('assets/index', 'AssetsController@index');
    Route::get('/asset/listing', 'AssetsController@listing');
    Route::get('/asset/add', 'AssetsController@addAssetForm');


    Route::get('workorder/index', 'WorkorderController@index');
    Route::get('/workorder/listing', 'WorkorderController@listing');
    Route::get('/workorder/add', 'WorkorderController@add');
    Route::post('/workorder/store', 'WorkorderController@store');

    Route::post('/get/flight', 'WorkorderController@getFlight');
    Route::post('/get/locations', 'WorkorderController@getLocations');
    Route::post('/get/coordinates', 'WorkorderController@getCoords');

    Route::get('/dashboard', 'DashboardController@index');
    // Route::get('/dashboard/updategraph1', 'DashboardController@DayWiseHeadcountGraph');
    Route::get('/dashboard/updategraph1', 'DashboardController@TotalHeadcountperDayGraph');
    Route::get('/dashboard/daywisedoorcountGraph', 'DashboardController@DayWiseDoorCountsGraph');






    Route::get('/userlisting', 'UserController@index');
    Route::get('/userlisting/get', 'UserController@listing');
    Route::get('/adduser', 'UserController@addForm');
    Route::post('/user_store','UserController@store');
    Route::get('/edituser', 'UserController@getEditFormUser');
    Route::post("/removeuser",'UserController@removeUser');

    Route::get('/jklisting','JKController@index');
    Route::get('/jklisting/get', 'JKController@listing');
    Route::get('/addjk', 'JKController@addJamatKhana');
    Route::post('/jk_store','JKController@store');
    Route::get('/getjks_sectorwise', 'JKController@getjks_sectorwise');
    Route::get('getAssigned_jks', 'JKController@getAssignedJamatKhanas_User');
    Route::get('/jk/editJK', 'JKController@getEditFormJK');
    Route::post("/jk/removejk",'JKController@removeJamatKhana');

    Route::get('/addsector', 'JKController@addSectorForm');
    Route::post('/sector_store', 'JKController@Sector_store');


    Route::get("/assign_users_to_jk","UserAssignmentController@index");
    Route::post("/assign_users_now","UserAssignmentController@store");


    Route::get('/schedule_management','ScheduleController@index');
    Route::get('/getScheduleOfJK', 'ScheduleController@getSchedule');
    Route::post('/submitSchedule', 'ScheduleController@validaeSchedule');
    Route::post('/WriteSchedule', 'ScheduleController@createSchedule');

    Route::get('/roles_permission_relation', 'RolePermissionController@index');
    Route::get('/getRolewisePermissions','RolePermissionController@getRolePermissions');
    Route::post('/submit_role_permissions','RolePermissionController@save');


    Route::get('/userMenuRelation', 'MenuPermissionController@index');
    Route::get('/getMenuwisePermissions','MenuPermissionController@getMenuPermissions');
    Route::post('/submit_menu_permissions','MenuPermissionController@save');

    Route::get('/address/get_states','AddressController@ajaxGetStates');
    Route::get('/address/get_cities','AddressController@ajaxGetCities');

});
