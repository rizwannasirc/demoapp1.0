<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('unauthorized' , function(){
    return "Un-Authorized access denied.";
});

Route::get('/home-page' , "AppController@getHomePage");
Route::get('/privacy' , "AppController@getPrivacy");


Route::middleware(['App_Auth'])->group(function () {



Route::get('/getuser' , "AppController@getuser");

Route::get('/userjks','AppController@jk_user');
Route::get("/validate", 'AppController@validateUser');
Route::get("/updateUsers", 'AppController@updateOnlineUsers');


// Route::get('/getJKs','AppController@getJks');
Route::get('/getJKDoors','AppController@getJKDoors');

Route::get('/DoorLogs','AppController@doorLoger');

Route::get('/getHeadcount','AppController@getHeadcountUpdate');
Route::get('/updateHeadcount','AppController@updatetHeadcount');

Route::get('/submitSurvey','SurveyController@store');

Route::get('/updateParking','AppController@updateParking');

});