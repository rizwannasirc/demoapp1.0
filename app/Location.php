<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    public function workorders()
    {
        return $this->belongsToMany('App\Workorder'); 
    }
}
