<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{
    public function validateUser(Request $request){

        $post= $request->all()['params'];
        $uid = $post['uid'];
        $password = $post['password'] ;
        
        $user = User::where('login_id1',$uid)->first();
        if($user === null)
        {
            return json_encode(['type'=>'error','msg'=>'Username does not exists. Please try with a valid username.']);
            exit;
        }

        $hashedPassword = $user->password;

        if(!Hash::check($password,$hashedPassword))
        {
            return json_encode(['type'=>'error','msg'=>'Invalid Password']);
            exit;
        }

        if(!$user->web_access)
        {
            return json_encode(['type'=>'error','msg'=>'You are not authorized user. Please contact Administrator.']);
            exit;
        }


        session(['uid'=>$user->id]);
        session(['user_firstname'=>$user->firstname]);
        session(['user_avatar'=>$user->avatar]);
        if($user->role_id)
            session(['role_id'=>$user->role_id]);
        else
            session(['admin'=>$user->is_admin]);

        return json_encode(['type'=>'success','route'=> url('/dashboard') ]);


    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return 'success';
    }


    public function Auth2validate(Request $request)
    {
        $email = $request->email;
        
        $user = User::where( function ($query) use ($email) {
            $query->where('login_id1',$email)->orWhere('login_id2',$email);
            
        })->where('web_access',1)->where('status',1)->first();


        if(!empty($user))
        {
            
            session(['uid'=>$user->id]);
            session(['user_firstname'=>$user->firstname]);
            session(['user_avatar'=>$user->avatar]);
            if($user->role_id)
                session(['role_id'=>$user->role_id]);
            else
                session(['admin'=>$user->is_admin]);
            $params = '';
            $route='';
            
           
            if(empty($user->menus->toArray()))
            {
                $route = url('/error_404');
                $params = "/" . "Menu Exception" . "/" . "No menu is assinged to you.";
            }
            else
            {
                $menus = $user->menus->toArray();
                $route = url($menus[0]['menu_route']);
            }  
            

            return json_encode(['action'=>'success' , 'message'=>'Please wait!', 'route'=>$route,'params'=>$params]);
        }
        else
        {
            return ['action'=>'error' , 'message'=>'You are not authorized user to access web panel.'];
        }
    }

    public function getToken(Request $request) {


        
        $code = $request->code;

        $data = array (
            'code'=> $code,
            'client_id'=>'a146e327-2265-4aa0-bbe5-87566f86b8bc',
            'redirect_uri'=>'https://headcountpro.com/ms_authorize',
            'scope'=>'user.read',
            'client_secret'=>'XlslYob-Lf9q.fqnc._t~B5ao7L_Fnv64P',
            'grant_type'=>'authorization_code',
        );

        if(!$request->session()->has('microsoft_token'))
        {
            $response = Http::asForm()->post('https://login.microsoftonline.com/common/oauth2/v2.0/token',  $data);

            $response = json_decode($response);

            if(isset($response->error))
            {
                return redirect('/')->with(['alert'=>['action'=>'error','message'=>'Microsoft session has expired. Please try again.']]);
            }
            else
            {
                session(['microsoft_token'=> json_encode($response) ]);
            }
        }
        else
        {
            $response = json_decode(session('microsoft_token'));
        }
        
      
        $access_token = $response->access_token;
        $token_type = $response->token_type;
        // $scope = $response->scope;


        $LoginAttempt = Http::withToken($access_token)->get("https://graph.microsoft.com/v1.0/me/");
        $LoginAttempt = $LoginAttempt->json();
        if(isset($LoginAttempt['userPrincipalName']))
        {
            $email = $LoginAttempt['userPrincipalName'];
        }
        else
        {
            $request->session()->flush();
            return redirect('/')->with(['alert'=>['action'=>'error','message'=>'Microsoft session has expired. Please try again.']]);
        }


        $user = User::where( function ($query) use ($email) {
            $query->where('login_id1',$email)->orWhere('login_id2',$email);
            
        })->where('web_access',1)->where('status',1)->first();


        if(!empty($user))
        {
            
            session(['uid'=>$user->id]);
            session(['user_firstname'=>$user->firstname]);
            session(['user_avatar'=>$user->avatar]);
            if($user->role_id)
                session(['role_id'=>$user->role_id]);
            else
                session(['admin'=>$user->is_admin]);
            $params = '';
            $route='';
            
           
            if(empty($user->menus->toArray()))
            {
                $route = url('/error_404');
                $params = "/" . "Menu Exception" . "/" . "No menu is assinged to you.";
            }
            else
            {
                $menus = $user->menus->toArray();
                $route = url($menus[0]['menu_route']);
            }  
           
            return redirect($route . $params);
            // return json_encode(['action'=>'success' , 'message'=>'Please wait!', 'route'=>$route,'params'=>$params]);
        }
        else
        {
            return redirect('/')->with(['alert'=>['action'=>'error','message'=>'You are not authorized user to access web panel.']]);
            // return ['action'=>'error' , 'message'=>'You are not authorized user to access web panel.'];
        }

    }

}
