<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Survey_log;
use App\Survey;
use App\User;
use App\Jamatkhana;

class SurveyController extends Controller
{




    public function store(Request $request)
    {
        $get = $request->all();

        $survey_id = $get['survey_id'];
        $user = $get['user'];
        $firstname = $get['firstname'];
        $lastname = $get['lastname'];
        $jkname = $get['jkname'];
        $role = $get['role'];
        $selectedDate = $get['selectedDate'];
        $data = $get['data'];

        $userDetail = User::where('login_id1',$user)->orWhere('login_id2',$user)->first();
        $user_id = $userDetail->id;

        $jk_detail = Jamatkhana::where('name',$jkname)->first();

        if(!empty($jk_detail))
            $jk_id = $jk_detail->id;
        

        $create =array(
            'survey_date'=>  date('Y-m-d',strtotime($selectedDate)),
            'survey_id'=>  $survey_id,
            'jamatkhana_id'=> $jk_id,
            'user_id'=> $user_id,
            'firstname'=>  $firstname,
            'lastname'=>  $lastname,
            'role'=> $role,
            'data'=> json_encode($data),
        );

        $attempt = Survey_log::create($create);
      
            return "success";
     

    }
}
