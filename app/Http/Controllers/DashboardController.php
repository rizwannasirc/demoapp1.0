<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Role;
use App\Lead;



class DashboardController extends Controller
{

    protected $menus;
    protected $uid;
    protected $user;
    protected $RolePermissions;

    public function __construct()
    {
      
        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','role_id','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }

            if($this->user->is_admin != 1)
            {
                $RolePermissions = Role::find($this->user->role_id)->permissions;
                $this->RolePermissions = array();
                foreach ($RolePermissions as $permission)
                {
                    array_push($this->RolePermissions,$permission->slug);
                }
            }
            
        return $next($request);
        });
    }

    public function index()
    {

    
        return view('main',[
                                    'menus'=> json_encode($this->menus),
                                    'user'=> json_encode($this->user),
                                    'page_title' => 'Dashboard',
                                    'page' => 'dashboard',
                                    'active_menu'=>1,
                                    'extra_info'=>json_encode([
                                        'RolePermissions'=> $this->RolePermissions,
                                        'updateGraph1'=> url('/dashboard/updategraph1'),
                                        'daywisedoorcounts'=> url('dashboard/daywisedoorcountGraph')
                                    ]),
                                    
                                ]);
    }


    public function DayWiseHeadcountGraph(Request $request)
    {
        date_default_timezone_set('Canada/Eastern');
        $jk_id = $request->jk_id;
        $dateRange = json_decode($request->daterange);
        $doors = $request->doors;
        $days = json_decode($request->days);
        

        if($this->user->is_admin ==1)
            $jkList = Jamatkhana::where('is_active',1)->get();
        else
            $jkList = $this->user->Jamatkhanas;

        if(!empty($jkList->toArray()))
        {
            $jkIds = array();
            
            foreach($jkList as $jk)
            {
                array_push($jkIds,$jk->id);
                $jknames[$jk->id] = $jk->name;
            }

            
            $Leads = Lead::selectRaw("jk_id,CONCAT(HOUR(created_at), ':00') AS Hours, avg(`cumulative_total`) as Total")->whereIn('jk_id',$jkIds);
            // $Leads = $Leads->
            // $Leads = $Leads->avg('cumulative_total');
            $Leads = $Leads->whereBetween('created_at',['2020-07-28 00:00:00','2020-08-18 23:59:59'])
                        ->groupBy(['Hours','jk_id']);

            $Leads = $Leads->get();

            $data = array();
            $x_axis = array();
            // $min_x_axix = $max_x_axix = 0;
            // print_r($jknames);

            foreach($Leads as $Lead)
            {
                //Getting Minimum Hours for X-Axis
                if(!isset($min_x_axis) )
                    $min_x_axis = $Lead->Hours;
                else{
                    if($min_x_axis > $Lead->Hours)
                        $min_x_axis = $Lead->Hours;
                }

                //Getting Maximum Hours for X-Axis
                if(!isset($max_x_axis) )
                    $max_x_axis = $Lead->Hours;
                else{
                    if($max_x_axis < $Lead->Hours)
                        $max_x_axis = $Lead->Hours;
                }

                for($i = $min_x_axis; $i <= $max_x_axis; $i=date('H:i',strtotime($i)+3600))
                {
                    if(!in_array($i, $x_axis))
                        array_push($x_axis,$i);
                }

                foreach($x_axis as $value)
                {
                    if($value == $Lead->Hours)
                    {
                        $data[$jknames[$Lead->jk_id]][] = round($Lead->Total,0);
                    }

                    
                        
                }

                // Populating data into array
                
                    
            }

            return json_encode(['graphdata'=>array_values($data),'legends'=>array_keys($data),'xaxis'=>$x_axis]);

        }

        



    }


    public function DayWiseDoorCountsGraph(Request $request)
    {
        date_default_timezone_set('Canada/Eastern');
        
        $jk_id = $request->jk_id;
        $dateRange = json_decode($request->daterange);
        $doors = $request->doors;
        $days = json_decode($request->days);
        

        if($this->user->is_admin ==1){
            $jkList = Jamatkhana::where('is_active',1);
            if($jk_id != 0)
            {
                $jkList = $jkList->where('id',$jk_id);
            }
            $jkList = $jkList->get();
        }
        else
            $jkList = $this->user->Jamatkhanas;

        if(!empty($jkList->toArray()))
        {
            $jkIds = array();
            
            foreach($jkList as $jk)
            {
                array_push($jkIds,$jk->id);
                $jknames[$jk->id] = $jk->name;
            }

            
            $Leads = Lead::selectRaw("jk_id,CONCAT('Door ',door) AS Doors,DATE_FORMAT(created_at,'%a') as Day,CONCAT(HOUR(created_at), ':00') AS Hours, avg(`cumulative_total`) as Total")->whereIn('jk_id',$jkIds);
            
            if($doors != 'All')
                $Leads = $Leads->where('door',$doors);
            // $Leads = $Leads->whereRaw("DATE_FORMAT(created_at,'%a')",$days);
            $Leads = $Leads->whereBetween('created_at',[$dateRange[0],$dateRange[1] ])
                        ->groupBy(['Hours','Doors','Day','jk_id']);

            $Leads = $Leads->get();
            
            // echo "<pre>";
            // print_r($Leads->toArray());

            // exit;
           
            $data = $jamatKhana = array();
            $x_axis = array();
            // $min_x_axix = $max_x_axix = 0;
            // print_r($jknames);
            

            foreach($Leads as $Lead)
            {
                //Getting Minimum Hours for X-Axis
                if(!isset($min_x_axis) )
                    $min_x_axis = $Lead->Hours;
                else{
                    if($min_x_axis > $Lead->Hours)
                        $min_x_axis = $Lead->Hours;
                }

                //Getting Maximum Hours for X-Axis
                if(!isset($max_x_axis) )
                    $max_x_axis = $Lead->Hours;
                else{
                    if($max_x_axis < $Lead->Hours)
                        $max_x_axis = $Lead->Hours;
                }

                for($i = $min_x_axis; $i <= $max_x_axis; $i=date('H:i',strtotime($i)+3600))
                {
                    if(!in_array($i, $x_axis))
                        array_push($x_axis,$i);
                }

                foreach($x_axis as $value)
                {
                    if($value == $Lead->Hours)
                    {
                       
                        // if(!in_array($jknames[$Lead->jk_id],$jamatKhana))
                        //     $jamatKhana[] = $jknames[$Lead->jk_id];
                        if(in_array($Lead->Day,$days))
                            $data[$jknames[$Lead->jk_id]][$Lead->Doors][] = round($Lead->Total,0);
                    }

                    
                        
                }

                // Populating data into array
                
                
                    
            }
            $data2 = [];
            foreach($data as $key => $doorDetail)
            {
                // print_r($item);
                foreach($doorDetail as $doorKey => $counts)
                {
                    $data2[$key . "," . $doorKey ] = $counts;
                }
            }
            // // $jk_Keys = array_keys($data2);
            // print_r($data2);
            return json_encode(['graphdata'=>array_values($data2),'legends'=>array_keys($data2),'xaxis'=>$x_axis]);

        }

        



    }

    public function TotalHeadcountperDayGraph(Request $request)
    {
        date_default_timezone_set('Canada/Eastern');

        $jk_id = $request->jk_id;
        $dateRange = json_decode($request->daterange);
        $doors = $request->doors;
        $days = json_decode($request->days);
        

        if($this->user->is_admin ==1)
        {
            $jkList = Jamatkhana::where('is_active',1);
            if($jk_id != 0)
            {
                $jkList = $jkList->where('id',$jk_id);
            }
            $jkList = $jkList->get();
        }
        else
            $jkList = $this->user->Jamatkhanas;

        if(!empty($jkList->toArray()))
        {
            $jkIds = array();
                
            foreach($jkList as $jk)
            {
                array_push($jkIds,$jk->id);
                $jknames[$jk->id] = $jk->name;
            }
    
            
            $Leads = Lead::selectRaw("jk_id, DATE_FORMAT(created_at,'%Y-%m-%d') AS created_date,DATE_FORMAT(created_at,'%a') as Day, max(`cumulative_total`) as Total")->whereIn('jk_id',$jkIds);
            // $Leads = $Leads->
            // $Leads = $Leads->avg('cumulative_total');
            $Leads = $Leads->whereBetween('created_at',[$dateRange[0],$dateRange[1]])
                        ->groupBy(['created_date','Day','jk_id']);

            $Leads = $Leads->get();
    
            $data = array();
            $x_axis = array();
            // $min_x_axix = $max_x_axix = 0;
            // echo "<pre>";
            // print_r($Leads->toArray());

            foreach($Leads as $key => $Lead)
            {
                // echo $Lead->Total;
                if(!in_array($Lead->created_date, $x_axis))
                        array_push($x_axis,$Lead->created_date);

                // if(in_array($$x_axis))
                // {
                    if(in_array($Lead->Day,$days))
                        $data[$jknames[$Lead->jk_id]][$Lead->created_date] = round($Lead->Total,0);

                    


                // }
                // else
                // {
                //     $data[$jknames[$Lead->jk_id]][] = '';
                // }
                // $data[$jknames[$Lead->jk_id]][] =  round($Lead->Total,0);
                // for($i = $min_x_axis; $i <= $max_x_axis; $i=date('Y-m-d',strtotime($i)+86400))
                // {
                    
                // }

                //Getting Minimum Hours for X-Axis
                // if(!isset($min_x_axis) )
                //     $min_x_axis = $Lead->Days;
                // else{
                //     if($min_x_axis > $Lead->Days)
                //         $min_x_axis = $Lead->Days;
                // }

                //Getting Maximum Hours for X-Axis
                // if(!isset($max_x_axis) )
                //     $max_x_axis = $Lead->Days;
                // else{
                //     if($max_x_axis < $Lead->Days)
                //         $max_x_axis = $Lead->Days;
                // }

                // for($i = $min_x_axis; $i <= $max_x_axis; $i=date('Y-m-d',strtotime($i)+86400))
                // {
                //     if(!in_array($i, $x_axis))
                //         array_push($x_axis,$i);
                // }

                // foreach($x_axis as $key => $value)
                // {
                //     if($value == $Lead->Days)
                //     {
                //         $data[$jknames[$Lead->jk_id]][] = round($Lead->Total,0);
                //     }
                //     else
                //     {
                //         $data[$jknames[$Lead->jk_id]][] = '';
                //     }
    
                    
                        
                // }

                // Populating data into array
                
                    
            }
            $data2 = [];

            foreach($x_axis as $key => $value)
            {
                foreach($data as $jk => $value2)
                {
                    // print_r($value2);
                    if(\key_exists($value,$value2))
                    {
                        $data2[$jk][] = $value2[$value];
                    }
                    else
                    {
                        $data2[$jk][] =  null;
                    }
                }
            }
    
            return json_encode(['graphdata'=>array_values($data2),'legends'=>array_keys($data2),'xaxis'=>$x_axis]);

        }

        
    }



}
