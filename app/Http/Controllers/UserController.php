<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Role;

class UserController extends Controller
{

    protected $menus;
    protected $uid;
    protected $user;
    protected $RolePermissions;

    public function __construct()
    {
      
        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','role_id','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }

            if($this->user->is_admin != 1)
            {
                $RolePermissions = Role::find($this->user->role_id)->permissions;
                $this->RolePermissions = array();
                foreach ($RolePermissions as $permission)
                {
                    array_push($this->RolePermissions,$permission->slug);
                }
            }
            
        return $next($request);
        });
    }

    public function index(){

        return view('main',[  
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'User Management',
            'page' => 'userlisting',
            'active_menu'=>2,
            'extra_info'=>json_encode([
                'RolePermissions'=> $this->RolePermissions,
                'add_user_route'=> url('/adduser'),
                'edit_user_route'=>url('/edituser'),
                'delete_user_route'=>url('/removeuser'),
            ]),
        ]);
    }



    public function listing()
    {
        $is_admin =  $this->user->is_admin;
            
        $data = User::where('status',1);

        if($is_admin != 1){
            $data = $data->whereNull('is_admin')->orWhere('is_admin', '!=' , 1);
            $data = $data->where('role_id','<',$this->user->role_id);
        }
        
        $data = $data->get();

        return json_encode($data);
    }

    public function addForm()
    {

        $is_admin = session('admin');
        $is_admin = '';
        $roles = Role::where('status',1);

        if($is_admin != 1)
            $roles = $roles->whereNull('is_superadmin')->orWhere('is_superadmin', '!=' , 1);

        $roles = $roles->get();
       
        $roles = array_map(function($items){
            
            $items['role_name'] = ucwords($items['role_name']);
            return $items;
           
        },$roles->toArray());


        return view('main',[
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'User Management',
            'page' => 'useraddform',
            'active_menu'=>2,
            'extra_info'=>json_encode([
                'back_route'=> url('/userlisting'),
                'submit_form' => url('/user_store'),
                'roles'=> $roles,
            ]),
        ]);
    }


    public function store(Request $request)
    {

      
        $post = $request->all() ;
        $uid = $request->uid;

        if(!empty($uid))
        {
            $validatedData = $request->validate([
                'login_id1' => 'required|email:rfc,dns|unique:users,login_id1,'. $uid,

            ]);
        }
        else
        {
            $validatedData = $request->validate([
                'login_id1' => 'required|email:rfc,dns|unique:users',
            ]);
        }

        $validatedData = $request->validate([
            'firstname'=> 'required',
            'lastname'=> 'required',
        ]);
        
        if(!empty($post->login_id2))
        {
            if(!empty($uid))
            {
                $validatedData = $request->validate([
                    'login_id2' => 'email:rfc,dns|unique:users,login_id2,'. $uid,

                ]);
            }
            else
            {
                $validatedData = $request->validate([
                    'login_id2' => 'email:rfc,dns|unique:users',
                ]);
            }
        }
        
        if(empty($post['role_id']['value']))
        {
            return json_encode(['message'=>'The Role is required.','action'=>'error']);
            exit;
        }
         
        $data = array('login_id1'=> strtolower($post['login_id1']),
                    'login_id2'=>   !empty($post['login_id2']) ? strtolower($post['login_id2']) : null ,
                    'firstname'=> ucwords($post['firstname']),
                    'lastname'=> ucwords($post['lastname']),
                    'web_access'=>$post['web_access']??'0',
                    'app_access'=>$post['app_access']??'0',
                    'is_admin'=>$post['super_admin']??0,
                    'role'=>$post['role_id']['text']??'',
                    'role_id'=>$post['role_id']['value']??0,
                    'created_by'=>$this->user->id,
                    'updated_by'=>$this->user->id,
                    );

        $create = User::updateOrCreate(['id'=>$uid],$data);
        
        return !empty($uid) ? "Updated" :"Success";
        
    }


    public function getEditFormUser(Request $request)
    {
        $uid= $request->uid;

        $user = User::find($uid);
        $roleList = Role::where('status',1);
        if($this->user->is_admin != 1)
            $roleList = $roleList->whereNull('is_superadmin')->orWhere('is_superadmin', '!=' , 1);

        $roleList = $roleList->get();
        $roleList = array_map(function($items){
            
            $items['role_name'] = ucwords($items['role_name']);
            return $items;
           
        },$roleList->toArray());


        if(!empty($user)) {
            
            

           

            return view('main',[
                'menus'=> json_encode($this->menus),
                'user'=> json_encode($this->user),
                'page_title' => 'User Management',
                'page' => 'useraddform',
                'active_menu'=>2,
                'extra_info'=>json_encode([
                    'form_title'=>'Edit User (' . $user->firstname . " " . $user->lastname .  ')',
                    'back_route'=> url('/userlisting'),
                    'submit_form' => url('/user_store'),
                    'roles'=> $roleList,
                    'editFormDetails' => array (
                       'user' => $user
                    ),
                ]),
            ]);

        };
        


    }


    public function removeUser(Request $request)
    {

        
        $uid = $request->uid;

        $user = User::find($uid);
        
        $user->delete();

        if ($user->trashed()) {
            return json_encode('success');
        }
        else
            return json_encode('fail');
    }


}
