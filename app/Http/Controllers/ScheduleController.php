<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Role;
use App\Jamatkhana;
use App\Sector;
use App\Jkschedule;
use DatePeriod;
use DateTime;
use DateInterval;
use DB;

class ScheduleController extends Controller
{
    protected $menus;
    protected $uid;
    protected $user;

    public function __construct()
    {
      
        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }
            
        return $next($request);
        });
    }

    public function index()
    {

        $sectors = Sector::all();

      

        return view('main',[
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'Jamat Khana Schedule Mangement',
            'page' => 'schedulepage',
            'active_menu'=>5,
            'extra_info'=>json_encode([
                'getSchedule_route'=> url('/getScheduleOfJK'),
                'getJks_sectorwise_route'=> url('/getjks_sectorwise'),
                'SubmitForm'=> url('/submitSchedule'),
                'WriteDB_Route'=> url('/WriteSchedule'),
                'sectors' => json_encode($sectors),
            ]),
        ]);
    }

    public function getSchedule(Request $request) 
    {
        $date_range = $request->date_range;
        $sector_id = $request->sector;
        $jk_id = $request->jamatkhana_id;

        $jkObject = Jamatkhana::find($jk_id);

        $dateArray = explode(" ~ ",$date_range);

        $start_date = $dateArray[0];
        $end_date = $dateArray[1];
       
       
        // $end_dateObject =  new DateTime( $end_date);
        // $end_dateObject->modify("+1 day");
        // $period = new DatePeriod(
        //     new DateTime($start_date),
        //     new DateInterval('P1D'),
        //     $end_dateObject
        // );

        $data = array();
        $headers = array(['text'=>'Time / Date', 'value'=>'time']);
        // foreach($period as $key => $date)
        // {   

        //     $data[$date->format("Y-m-d")]['start'] = "";
        //     $data[$date->format("Y-m-d")]['end'] = "";  

        // }
        
        $scheduleList = Jkschedule::whereBetween('schedule_date',[$start_date,$end_date])
                    ->where('jamatkhana_id',$jk_id)
                    ->orderBy('schedule_date')
                    ->get();
        if(!empty($scheduleList)){

            $startTimings  =array('start'=>array(),'end'=>array());

            foreach($scheduleList as $schedule)
            {

                

                array_push($startTimings['start'],array($schedule->id.$schedule->schedule_date => date("H:i",strtotime($schedule->schedule_from))) );
                array_push($startTimings['end'], array($schedule->id.$schedule->schedule_date => date("H:i",strtotime($schedule->schedule_to))) );
             
                array_push($headers,['text'=>$schedule->schedule_date,'value'=>$schedule->id.$schedule->schedule_date]);
            }
        }
        
        // $data[] =  array_merge($startTimings['start']) ;
        $startTime = array('time'=>'Start Time');
        foreach ($startTimings['start'] as $key => $value)
        {
        
            $startTime = array_merge($startTime, $value);
        }


        $endTime = array('time'=>'Closing Time');
        foreach ($startTimings['end'] as $key => $value)
        {
        
            $endTime = array_merge($endTime, $value);
        }



        $data = array($startTime,$endTime);


        return json_encode(['data'=>$data,'headers'=>$headers]);
    }

    public function validaeSchedule(Request $request)
    {
        $request = $request->data;
        $JKDetails = Jamatkhana::find($request['jk_id']);

        date_default_timezone_set($JKDetails->timezone);

       
        $Monday_from = $request['mon_from'];
        $Tuesday_from = $request['tue_from'];
        $Wednesday_from = $request['wed_from'];
        $Thursday_from = $request['thur_from'];
        $Friday_from = $request['fri_from'];
        $Saturday_from = $request['sat_from'];
        $Sunday_from = $request['sun_from'];

        $Monday_to = $request['mon_to'];
        $Tuesday_to = $request['tue_to'];
        $Wednesday_to = $request['wed_to'];
        $Thursday_to = $request['thur_to'];
        $Friday_to = $request['fri_to'];
        $Saturday_to = $request['sat_to'];
        $Sunday_to = $request['sun_to'];

        $eventType = $request['eventType'];
        $comments = $request['comments'];
        $jk_id  = $request['jk_id'];
        $dateRange = $request['dateRange'];

        $dateArray = explode(" ~ ",$dateRange);

        $start_date = $dateArray[0];
        $end_date = $dateArray[1];
        

        $end_dateObject =  new DateTime( $end_date);
        $end_dateObject->modify("+1 day");
        $period = new DatePeriod(
            new DateTime($start_date),
            new DateInterval('PT30M'),
            $end_dateObject
        );
        $error = array();

        foreach($period as $key => $date)
        {
            $day = $date->format("l");

            $getTempSchedule = Jkschedule::where('schedule_date', $date->format('Y-m-d'))->where('jamatkhana_id',$jk_id)->get();
            
            if(!empty($getTempSchedule->toArray()))
            {
                // echo $date->format("H:i");

                foreach ($getTempSchedule as $key => $schedule)
                {
                    if($day == 'Monday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Monday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Monday_to .":00");
                    }

                    if($day == 'Tuesday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Tuesday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Tuesday_to .":00");
                    }
                    
                    if($day == 'Wednesday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Wednesday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Wednesday_to .":00");
                    }

                    if($day == 'Thursday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Thursday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Thursday_to .":00");
                    }

                    if($day == 'Friday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Friday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Friday_to .":00");
                    }

                    if($day == 'Saturday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Saturday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Saturday_to .":00");
                    }

                    if($day == 'Sunday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Sunday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Sunday_to .":00");
                    }

                    if( strtotime($date->format("Y-m-d H:i:00")) >= $requestTimeFrom  &&   strtotime($date->format("Y-m-d H:i:00")) < $requestTimeTo)
                    {
                        // echo $date->format("Y-m-d H:i:00");
                        if(strtotime($date->format("Y-m-d H:i:00")) >= strtotime($schedule->schedule_from))
                        {
                            // echo $schedule->schedule_from;
                            $error[$date->format("Y-m-d")] = array(
                                'action'=>'fail',
                                'msg'=> "This Jamat Khana is already scheduled from $schedule->schedule_from to $schedule->schedule_to ."
                            );
                            // continue;
                        }
                    }    
                }

                // print_r($getTempSchedule->toArray());
            }
            
        }

        // print_r($error);
        if(!empty($error))
            return array_values($error);
        else {
            return 'success';
        }

        exit;
        foreach($period as $key => $date)
        {   
            $day = $date->format("l");

            $getTempSchedule = Jkschedule::where('schedule_date', $date->format('Y-m-d'))->where('jamatkhana_id',$jk_id)->get();

            if( !empty($getTempSchedule->toArray()) )
            {
                
                
                foreach ($getTempSchedule as $key => $schedule)
                {
                    $timeStart = strtotime($schedule->schedule_from);
                    $timeEnd = strtotime($schedule->schedule_to);

                    if($day == 'Monday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Monday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Monday_to .":00");
                    }
                    if($day == 'Tuesday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Tuesday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Tuesday_to .":00");
                    }
                    
                    if($day == 'Wednesday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Wednesday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Wednesday_to .":00");
                    }

                    if($day == 'Thursday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Thursday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Thursday_to .":00");
                    }

                    if($day == 'Friday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Friday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Friday_to .":00");
                    }

                    if($day == 'Saturday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Saturday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Saturday_to .":00");
                    }

                    if($day == 'Sunday') {
                        $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Sunday_from .":00");
                        $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Sunday_to .":00");
                    }
                  
                    
                    if( $requestTimeFrom >= $timeStart  && $requestTimeFrom < $timeEnd  )
                    {
                        $error[] = array(
                            'action'=>'fail',
                            'msg'=> "This Jamat Khana is already scheduled from $schedule->schedule_from to $schedule->schedule_to ."
                        );
                    }
                    
                   
                }

            }

        }
        if(!empty($error))
            return $error;
        else {
            return 'success';
        }
        


    }

    public function createSchedule(Request $request)
    {
        // $request = $request->data;

       
        $JKDetails = Jamatkhana::find($request->jk_id);

        date_default_timezone_set($JKDetails->timezone);

        $Monday_from = $request['mon_from'];
        $Tuesday_from = $request['tue_from'];
        $Wednesday_from = $request['wed_from'];
        $Thursday_from = $request['thur_from'];
        $Friday_from = $request['fri_from'];
        $Saturday_from = $request['sat_from'];
        $Sunday_from = $request['sun_from'];

        $Monday_to = $request['mon_to'];
        $Tuesday_to = $request['tue_to'];
        $Wednesday_to = $request['wed_to'];
        $Thursday_to = $request['thur_to'];
        $Friday_to = $request['fri_to'];
        $Saturday_to = $request['sat_to'];
        $Sunday_to = $request['sun_to'];

        $eventType = $request['eventType'];
        $comments = $request['comments'];
        $jk_id  = $request['jk_id'];
        $dateRange = $request['dateRange'];

        $dateArray = explode(" ~ ",$dateRange);

        $start_date = $dateArray[0];
        $end_date = $dateArray[1];
        

        $end_dateObject =  new DateTime( $end_date);
        $end_dateObject->modify("+1 day");
        $period = new DatePeriod(
            new DateTime($start_date),
            new DateInterval('PT30M'),
            $end_dateObject
        );

        foreach($period as $key => $date)
        {
            $day = $date->format("l");

            if($day == 'Monday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Monday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Monday_to .":00");
            }

            if($day == 'Tuesday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Tuesday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Tuesday_to .":00");
            }
            
            if($day == 'Wednesday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Wednesday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Wednesday_to .":00");
            }

            if($day == 'Thursday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Thursday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Thursday_to .":00");
            }

            if($day == 'Friday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Friday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Friday_to .":00");
            }

            if($day == 'Saturday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Saturday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Saturday_to .":00");
            }

            if($day == 'Sunday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Sunday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Sunday_to .":00");
            }

            $getTempSchedule = Jkschedule::where('schedule_date', $date->format('Y-m-d'))->where('jamatkhana_id',$jk_id)->get();
            
            if(!empty($getTempSchedule->toArray()))
            {
                // echo $date->format("H:i");

                foreach ($getTempSchedule as $key => $schedule)
                {
                    

                    if( strtotime($date->format("Y-m-d H:i:00")) >= $requestTimeFrom  &&   strtotime($date->format("Y-m-d H:i:00")) < $requestTimeTo)
                    {
                        // echo $date->format("Y-m-d H:i:00");
                        if(strtotime($date->format("Y-m-d H:i:00")) >= strtotime($schedule->schedule_from))
                        {
                        //    $data = array(
                        //        'id'=>$schedule->id,
                        //        'schedule_from'=> date("Y-m-d H:i:s",$requestTimeFrom),
                        //        'schedule_to'=> date("Y-m-d H:i:s", $requestTimeTo)
                        //    );

                        //    Jkschedule::update($data);
                            Jkschedule::where('id',$schedule->id)->delete();
                           
                        }
                    }
                    
                    



                }

                
            }
           
           
            
        }

        $period = new DatePeriod(
            new DateTime($start_date),
            new DateInterval('P1D'),
            $end_dateObject
        );

        foreach($period as $key => $date)
        {
            $day = $date->format("l");

            if($day == 'Monday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Monday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Monday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Tuesday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Tuesday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Tuesday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                $requestTimeTo = $requestTimeTo + (24*60*60);
            }
            
            if($day == 'Wednesday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Wednesday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Wednesday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Thursday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Thursday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Thursday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Friday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Friday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Friday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Saturday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Saturday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Saturday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Sunday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Sunday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Sunday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            $data = array(
                'schedule_date'=> $date->format("Y-m-d"),
                'schedule_from'=> date('Y-m-d H:i:00',$requestTimeFrom) ,
                'schedule_to'=> date('Y-m-d H:i:00',$requestTimeTo),
                'day'=>$day,
                'event'=>$eventType,
                'comments'=>$comments,
                'jamatkhana_id'=>$jk_id,
                'event_status'=>1
            );

            if(!empty($requestTimeFrom))
                Jkschedule::create($data);


            // if(!empty($requestTimeFrom))
            //     print_r($data);

        }


        return 'success';

        exit;
        foreach($period as $key => $date)
        {  
            $day = $date->format("l");

            if($day == 'Monday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Monday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Monday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                    $requestTimeTo = $requestTimeTo + (24*60*60);
            }
            if($day == 'Tuesday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Tuesday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Tuesday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                    $requestTimeTo = $requestTimeTo + (24*60*60);
            }
            
            if($day == 'Wednesday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Wednesday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Wednesday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                    $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Thursday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Thursday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Thursday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                    $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Friday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Friday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Friday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                    $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Saturday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Saturday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Saturday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                    $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if($day == 'Sunday') {
                $requestTimeFrom = strtotime($date->format("Y-m-d") . " " . $Sunday_from .":00");
                $requestTimeTo = strtotime($date->format("Y-m-d") . " " . $Sunday_to .":00");
                if($requestTimeFrom > $requestTimeTo )
                    $requestTimeTo = $requestTimeTo + (24*60*60);
            }

            if(!empty($requestTimeFrom) )
            {
                $data = array(
                    'jamatkhana_id'=>$jk_id,
                    'schedule_date'=> $date->format("Y-m-d"),
                    'schedule_from'=> date("Y-m-d H:i:00", $requestTimeFrom ),
                    'schedule_to'=> date("Y-m-d H:i:00", $requestTimeTo ) ,
                    'day'=> $day,
                    'event'=>$eventType,
                    'comments'=>$comments,
                    'event_status'=>1,
                );
    
            }

            if(!empty($data))
            {
               $a =  Jkschedule::where('schedule_date',$date->format("Y-m-d"))
                            ->where('schedule_from','<=', date("Y-m-d H:i:00", $requestTimeFrom ) )
                            ->where('schedule_to', ">=",date("Y-m-d H:i:00", $requestTimeFrom ))
                            ->get();

                print_r($a->toArray());

                // Jkschedule::create( $data);
            }
            

        }

        return 'success';
    }
}
