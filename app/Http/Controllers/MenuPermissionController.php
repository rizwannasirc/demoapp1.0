<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Role;
use App\Permission;

class MenuPermissionController extends Controller
{
    public function __construct()
    {
      
        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }
            
        return $next($request);
        });
    }

    public function index(){

        $users = User::select('id','firstname','lastname')->where('status',1);

        if($this->user->is_admin != 1)
        {
            $users = $users->where('is_admin','!=', 1);
        }

        $users = $users->get();

        $users = $users->toArray();

        $users = array_map(function($user) {
            $user['firstname'] = ucwords($user['firstname']);
            $user['lastname'] = ucwords($user['lastname']);
            return $user;
        },$users);

        $menus = Menu::select(['id','menu_name'])->where('isactive',1);

        if($this->user->is_admin != 1)
        {
            $menus = $menus->where('is_superadmin','!=', 1);
        }

        $menus = $menus->get();

        // print_r(json_encode($menus));
        // exit;

        return view('main',[
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'User Menu Permissions',
            'page' => 'menu_permissions',
            'active_menu'=>7,
            'extra_info'=>json_encode([
                'form_submit'=> url('/submit_menu_permissions'),
                'get_menu_permission'=>url('/getMenuwisePermissions'),
                'users'=> json_encode($users),
                'permissions'=> json_encode($menus),
            ]),
        ]);
    }

    public function getMenuPermissions(Request $request) {
        
        $user_id = $request->user_id;

        $permissionList = User::find($user_id)->menus;

        return json_encode($permissionList->toArray());
    }

    public function save(Request $request)
    {
        $user_id = $request->user_id;
        $permissions = $request->permissions;

        $permission_ids  = array_map(function($a){
            if(isset($a['id']))
                return $a['id'];
            return null;
        },$permissions);

  
        if($user_id >0){
            $user = User::find($user_id);
            $user->menus()->detach();
            $user->menus()->attach($permission_ids);
        }

       
    }


}
