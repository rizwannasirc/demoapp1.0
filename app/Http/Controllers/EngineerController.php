<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Role;
use App\Engineer;

class EngineerController extends Controller
{

    protected $menus;
    protected $uid;
    protected $user;
    protected $RolePermissions;

    public function __construct()
    {
      
        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','role_id','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }

            if($this->user->is_admin != 1)
            {
                $RolePermissions = Role::find($this->user->role_id)->permissions;
                $this->RolePermissions = array();
                foreach ($RolePermissions as $permission)
                {
                    array_push($this->RolePermissions,$permission->slug);
                }
            }
            
        return $next($request);
        });
    }

    public function EngineerIndex(){
        return view('main',[  
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'Engineer title',
            'page' => 'Engineer_route',
            'active_menu'=>6,
            'extra_info'=>json_encode([
                'RolePermissions'=> $this->RolePermissions,
                'add_eng_route'=> url('/engineer/adduser'),
                'edit_eng_route'=>url('/edituser'),
                'delete_eng_route'=>url('/removeuser'),
                'listing'=>url('/eng/listing'),
            ]),
        ]);
    }

    public function listing(Request $request)
    {
        $is_admin =  $this->user->is_admin;
            
        $data = Engineer::where('status',1);
        
        $data = $data->get();

        return json_encode($data);
    }
}
