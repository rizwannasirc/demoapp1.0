<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Role;
use App\Jamatkhana;
use App\Sector;
use DB;


class UserAssignmentController extends Controller
{

    protected $menus;
    protected $uid;
    protected $user;
    protected $RolePermissions;

    public function __construct()
    {
      
        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','role_id','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }

            if($this->user->is_admin != 1)
            {
                $RolePermissions = Role::find($this->user->role_id)->permissions;
                $this->RolePermissions = array();
                foreach ($RolePermissions as $permission)
                {
                    array_push($this->RolePermissions,$permission->slug);
                }
            }
            
        return $next($request);
        });
    }

    public function index()
    {

        $sectors = Sector::all();
        $userlist = User::where('status',1);

        if($this->user->is_admin != 1)
        {
            $userlist = $userlist->where(function($query){
                $query->whereNull('is_admin')
                ->orWhere('is_admin', '!=' , 1);
            });

            $userlist = $userlist->where('role_id','<',$this->user->role_id );
        }
           
        $userlist = $userlist->where('id','!=',$this->user->id);
        $userlist = $userlist->get();





        return view('main',[
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'Making Users and Jamat Khana Relationship',
            'page' => 'jk_user_assignment',
            'active_menu'=>4,
            'extra_info'=>json_encode([
                'RolePermissions'=> $this->RolePermissions,
                'submit_button_route'=> url('/assign_users_now'),
                'getJks_sectorwise_route'=> url('/getjks_sectorwise'),
                'selected_jks_route' => url('/getAssigned_jks'),
                'sectors' => json_encode($sectors),
                'users' => json_encode($userlist),
            ]),
        ]);
    }

    public function store(Request $request)
    {
        $sector_id = $request->sector;
        $user_id = $request->user;
        $jklist = $request->jamat_khana_list;

        $validatedData = $request->validate([
            'sector' => 'required',
            'user'=> 'required',
        ]);
        
        DB::table('jamatkhana_user')->where('user_id',$user_id)->where('sector_id',$sector_id)->delete();
        
        if(!empty($jklist))
        {
            foreach($jklist as $key => $jk_id)
            {
                DB::table('jamatkhana_user')->insert(array(
                    'jamatkhana_id'=>$jk_id,
                    'user_id'=>$user_id,
                    'sector_id'=>$sector_id,
                ));
        
            }
        }
       
        return 'Success';
        // return [];
    }
}
