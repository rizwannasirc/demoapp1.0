<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Role;
use App\Permission;


class RolePermissionController extends Controller
{

    public function __construct()
    {
      
        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }
            
        return $next($request);
        });
    }

    public function index(){

        $roles = Role::select('id','role_name')->where('status',1);

        if($this->user->is_admin != 1)
        {
            $roles = $roles->where('is_superadmin','!=', 1);
            $roles = $roles->where('id',"<",$this->user->role_id);
        }

        $roles = $roles->get();

        $roles = $roles->toArray();

        $roles = array_map(function($role) {
            $role['role_name'] = ucwords($role['role_name']);
            return $role;
        },$roles);

        $permissions = new Permission();

        if($this->user->is_admin != 1)
        {
            $permissions = $permissions->where('is_superadmin','!=', 1);
        }

        $permissions = $permissions->get();

        return view('main',[
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'Role Permissions Management',
            'page' => 'role_permissions',
            'active_menu'=>6,
            'extra_info'=>json_encode([
                'form_submit'=> url('/submit_role_permissions'),
                'get_role_permission'=>url('/getRolewisePermissions'),
                'roles'=> json_encode($roles),
                'permissions'=> json_encode($permissions),
            ]),
        ]);
    }

    public function getRolePermissions(Request $request) {
        
        $role_id = $request->role_id;

        $permissionList = Role::find($role_id)->permissions;

        return json_encode($permissionList->toArray());
    }

    public function save(Request $request)
    {
        $role_id = $request->role_id;
        $permissions = $request->permissions;

        $permission_ids  = array_map(function($a){
            if(isset($a['id']))
                return $a['id'];
            return null;
        },$permissions);

  
        if($role_id >0){
            $role = Role::find($role_id);
            $role->permissions()->detach();
            $role->permissions()->attach($permission_ids);
        }

       
    }

}
