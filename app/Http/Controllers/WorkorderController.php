<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\User;
use App\Menu;
use App\Role;
use App\Workorder;
use App\Location;

class WorkorderController extends Controller
{
    protected $menus;
    protected $uid;
    protected $user;
    protected $RolePermissions;

    public function __construct()
    {

        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','role_id','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }

            if($this->user->is_admin != 1)
            {
                $RolePermissions = Role::find($this->user->role_id)->permissions;
                $this->RolePermissions = array();
                foreach ($RolePermissions as $permission)
                {
                    array_push($this->RolePermissions,$permission->slug);
                }
            }

        return $next($request);
        });
    }

    public function index(){

        return view('main',[
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'Assets Management',
            'page' => 'workorderlisting',
            'active_menu'=>10,
            'extra_info'=>json_encode([
                'RolePermissions'=> $this->RolePermissions,
                'add_asset_route'=> url('/workorder/add'),
                'edit_asset_route'=>url('/workorder/edit'),
                'delete_asset_route'=>url('/workorder/remove'),
                'listing'=>url('/workorder/listing'),
            ]),
        ]);
    }

    public function listing()
    {
        $is_admin =  $this->user->is_admin;

        $data = Workorder::where('status',1);

        // if($is_admin != 1){
        //     $data = $data->whereNull('is_admin')->orWhere('is_admin', '!=' , 1);
        //     $data = $data->where('role_id','<',$this->user->role_id);
        // }

        $data = $data->get();

        return json_encode($data);
    }

    public function add()
    {

        $users = User::where('role','Engineer')->orWhere('role_id',2)->get();

        return view('main',[
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'Work Orders Management',
            'page' => 'WorkorderAddform',
            'active_menu'=>10,
            'extra_info'=>json_encode([
                'back_route'=> url('/workorder/index'),
                'submit_form' => url('/workorder/store'),
                'engineers' => $users,
            ]),
        ]);
    }

    public function store(Request $request)
    {


        $post = $request->all() ;
        $uid = $request->uid;

        $validatedData = $request->validate([
            'date' => 'required'. $uid,
            'time' => 'required',
            'workorder_number'=> 'required|unique:workorders,work_order_no,'. $uid,
            'flight_number'=> 'required',
        ]);

        $eng_counts = count($request->engineers);


        $data = array('date_value'=> $post['date'],
                    'time_value'=> $post['time']   ,
                    'site_location'=> ucwords($post['sitelocation']),
                    'work_order_no'=> $post['workorder_number'],
                    'flight_no'=>$post['flight_number'],
                    'aircraft'=>$post['aircraft'],
                    'city'=>$post['city'],
                    'country'=>$post['country'],
                    'status'=>1,
                    'eng_id'=>$eng_counts,
                    );

        $create = Workorder::updateOrCreate(['id'=>$uid],$data);

        if($create->id > 0){
            $wk = Workorder::find($create->id);
            $wk->users()->detach();
            $wk->users()->attach($request->engineers);
        }

        return !empty($uid) ? "Updated" :"Success";

    }

    public function getFlight(Request $request){
        $flight = Http::get('http://api.aviationstack.com/v1/flights?access_key=9806bbeb7dc3a35f2501916e9ce17266&flight_iata='.$request['iata'])->json();
        return $flight;
    }

    public function getLocations(Request $request){
        $location = Location::where('workorder_id', (int)$request['id'])->get()->toArray();
        return json_encode($location);
    }

    public function getCoords(Request $request){
        $coords = Http::get('https://maps.googleapis.com/maps/api/geocode/json?address='.$request['address'].'&key='.env('MAP_KEY'));
        // echo 'https://maps.googleapis.com/maps/api/geocode/json?address='.$request['address'].'&key='.env('MAP_KEY');
        return $coords;
    }
}
