<?php

namespace App\Http\Controllers;

use App\Userdoorlog;
use App\Lead;
use App\Jamatkhana;
use App\User;
use App\Jkschedule;
use App\Parking_log;

use Illuminate\Http\Request;

class AppController extends Controller
{

    public function getHomePage()
    {
        return view('homepage');
    }

    public function getPrivacy()
    {
        return view('privacy.privacy_policy');
    }


    public function getuser(Request $request){
        $get = $request->all();
        $user_email = $get['user_id'];
        $userDetails = User::where('login_id1',$user_email)->orWhere('login_id2',$user_email)->first();
       
        
        return json_encode(['firstname'=>$userDetails->firstname,
                            'lastname'=>$userDetails->lastname,
                            'role'=>$userDetails->role,                 
        ]);

    }


    public function doorLoger(Request $request){

        $get = $request->all();
        $jk = $get['jk'];   

        $JKDetails = Jamatkhana::where('name', $jk)->first();

       
        $jk_id = $JKDetails->id;

        date_default_timezone_set($JKDetails->timezone);
        // echo $JKDetails->timezone;
        // echo date("Y-m-d H:i:s", time());
        // exit;
        
        $user_email = $get['user_id'];
        $door = $get['door'];
       
        $date = time();
        $time_now = date("Y-m-d H:i:s", $date + 300);
        $time_end = date("Y-m-d H:i:s", $date);
        // echo $time_now;

       


        $is_scheduled = Jkschedule::where('jamatkhana_id',$jk_id)->where('schedule_date', date("Y-m-d",$date) )->where('schedule_from', "<=" , ($time_now)  )->where('schedule_to', ">" , ($time_end)  )->where('event_status',1)->orderBy('schedule_from','asc')->first();

        // print_r($time_end );
        // exit;
        $open = 0;
        $operation = 0;
        $user_id = $is_door_occupied = 0;
        $schedule_id = 0;

        if(!empty($is_scheduled)){
            $open = 1;
            $schedule_id = $is_scheduled->id;
            $userDetails = User::where('login_id1',$user_email)->orWhere('login_id2',$user_email)->first();
            $user_id = $userDetails->id;

            //Checking if door is already occupied.
            $DoorDetails = Userdoorlog::where('jamatkhana_id',$jk_id)->where('schedule_id',$schedule_id)->where('door',$door)->first();
            $is_door_occupied = empty($DoorDetails) ? 0 : 1;

            if($is_door_occupied == 0) {
                //Checking if user is alraedy logged in
                $isuserLogged_in = Userdoorlog::where('user_id',$user_id)->where('jamatkhana_id',$jk_id)->where('schedule_id',$schedule_id)->first();
                if(!empty($isuserLogged_in) )
                { // if user is already logged in 
                    $isuserLogged_in->door = $door;
                    $isuserLogged_in->jamatkhana_id = $jk_id;
                    $isuserLogged_in->save();
                    $operation = 1;
                }
                else
                { //if there is no record in Doors log

                    Userdoorlog::where('user_id',$user_id)->delete();

                    $data = array('user_id'=> $user_id,
                                'jamatkhana_id'=>$jk_id,
                                'schedule_id'=>$is_scheduled->id,
                                'door'=>$door,
                            );
                    // print_r($data);
                    Userdoorlog::create($data);
                    $operation = 1;
                }
            }
            else
            {
                if( $DoorDetails->user_id == $user_id )
                {
                    $operation = 1;
                    $is_door_occupied = 0;
                }
            }
            

            

        }
            

        


        return ['operation'=>$operation,'open'=>$open, 'user_id'=>$user_id, 'jk_id'=>$jk_id , 'is_door_occupied'=> $is_door_occupied, 'door'=> intval( $door), 'schedule_id' => intval($schedule_id) ];
    }

    public function getHeadcountUpdate(Request $request)
    {
        $isParking = 0;
        $get = $request->all();
        $jk_id = $get['jk_id'];
        $door = $get['door'];
        $schedule_id = $get['schedule_id'];
        $counter = 0 ;
        $jamatkhana = Jamatkhana::where('id',$jk_id)->first();

        $isParking = $jamatkhana->parking_avaliable == 'Yes' ? 1 : 0 ;

        $query = Lead::where('jk_id', $jk_id)->where('schedule_id',$schedule_id)->orderBy('id','desc')->first();
        if(!empty($query))
            $counter= $query->cumulative_total;
        // foreach($query as $q)
        // {
        //     $counter += $q->counter;
        // }

        $onlineUsers = Userdoorlog::select(['users.firstname','users.lastname','userdoorlogs.door'])->where('jamatkhana_id', $jk_id)
                        ->join('users', 'users.id', '=', 'userdoorlogs.user_id');
        
        $parking = Parking_log::where('jkschedule_id',$schedule_id)->where('jamatkhana_id',$jk_id)->orderBy('id','desc')->first();

        if(!empty($parking))
            $isParking = $parking->is_available == 'true' ? 1 : 0; 


        return json_encode(['Headcount'=>$counter,'Capacity'=>$jamatkhana->allowed_capacity,'onlineUsers'=>$onlineUsers->count(), 'users'=>$onlineUsers->get(), 'parking'=>$isParking, 'parking_facility'=>$jamatkhana->parking_avaliable == 'Yes' ]);
    }

    public function updatetHeadcount(Request $request)
    {

        $get = $request->all();
        $jk_id = $get['jk_id'];
        $door = $get['door'];
        $counter = $get['counter'];
        $user_id = $get['user_id'];
        $schedule_id = $get['schedule_id'];
        $Total = $counter;
        $previousCumulative_total = 0;
        $process= true;
        $previousLead = Lead::where('jk_id',$jk_id)->where('schedule_id',$schedule_id)->orderBy('id','desc')->first();

        

        if(!empty($previousLead))
        {
            if($previousLead->cumulative_total != '')
            {
                $previousCumulative_total = $previousLead->cumulative_total;
            }
          
        }

        // echo $previousCumulative_total;
     
            $sum = $previousCumulative_total + $counter;
            if($sum < 0)
            {
                $Total = 0;
                if($previousCumulative_total > 0)
                $counter =  $counter - $sum;
                else
                $counter = 0;
            }
            else
                $Total = $previousCumulative_total + $counter;
        

        // echo "Counter: $counter and its total is $Total";

        
        
        $data = array('jk_id'=>$jk_id,
                        'door'=>$door,
                        'counter'=>$counter,
                        'user_id'=> $user_id,
                        'schedule_id'=> $schedule_id,
                        'cumulative_total'=> $Total);

       
        if($previousCumulative_total == 0 && $counter <=  0 )
            $process = false;

        if($process)
            $query = Lead::create($data);
      
       


        return $this->getHeadcountUpdate($request);
    }

    public function jk_user(Request $request)
    {

        $get = $request->all();
        $user = $get['user'];
        $userDetail = User::where('login_id1',$user)->orWhere('login_id2',$user)->first();
        $user_jks = $userDetail->jamatkhanas;

        $jks = array();

        foreach($user_jks as $user_jk){
            array_push($jks,$user_jk->name);
        }

        return json_encode($jks);




    }

    public function validateUser(Request $request)
    {
        $get = $request->all();
        $user_email = $get['user_id'];

        $userDetail = User::where("app_access",1)->where('login_id1',$user_email)->orWhere('login_id2',$user_email)->first();

        if(!empty($userDetail))
        {
            return json_encode(['role'=>$userDetail->role_id]);
        }
        else
            return "fail";

    }

    public function updateOnlineUsers(Request $request)
    {
        $get = $request->all();
        $user_email = $get['user_id'];

        return $user_email;

    }

    public function updateParking(Request $request){
        $get = $request->all();
        $user_id = $get['user_id'];
        $jk_id = $get['jk_id'];
        $parking = $get['parking'];
        $schedule_id = $get['schedule_id'];
        $JKDetails = Jamatkhana::find($jk_id);
        

        date_default_timezone_set($JKDetails->timezone);


        $data = array('jamatkhana_id'=> $jk_id,
                        'jkschedule_id'=>$schedule_id,
                        'is_available'=> $parking,
                        'initial_full'=>date('Y-m-d H:i:s', strtotime('now'))
        
        );
        
        Parking_log::create($data);


        return 'success';
    }


    public function getJKs(Requset $request)
    {

        $get = $request->all();
        $user_email = $get['user_email'];
        

        $jks = Jamatkhana::all();
        $list = [];
        foreach($jks as $jk)
        {
            array_push($list, $jk->name) ;
        }
        return $list;
    }

    public function getJKDoors(Request $request)
    {
        $get = $request->all();
        $id = $get['id'];
        $jks = Jamatkhana::where('name', $id)->first();
        
        return $jks->no_of_doors;
    }
}
