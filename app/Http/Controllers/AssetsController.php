<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Role;
use App\Asset;

class AssetsController extends Controller
{
    protected $menus;
    protected $uid;
    protected $user;
    protected $RolePermissions;

    public function __construct()
    {
      
        $this->middleware(function ($request, $next) {
            // fetch session and use it in entire class with constructor
            $this->uid =  session('uid');
            $this->user = User::select(['id','firstname','lastname','role','role_id','is_admin','avatar'])->find($this->uid);
            if($this->user->is_admin != 1)
                $this->menus =$this->user->menus;
            else {
                $this->menus = Menu::where('isactive',1)->get();
            }

            if($this->user->is_admin != 1)
            {
                $RolePermissions = Role::find($this->user->role_id)->permissions;
                $this->RolePermissions = array();
                foreach ($RolePermissions as $permission)
                {
                    array_push($this->RolePermissions,$permission->slug);
                }
            }
            
        return $next($request);
        });
    }

    public function index(){

        return view('main',[  
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'Assets Management',
            'page' => 'assetslisting',
            'active_menu'=>9,
            'extra_info'=>json_encode([
                'RolePermissions'=> $this->RolePermissions,
                'add_asset_route'=> url('/asset/add'),
                'edit_asset_route'=>url('/asset/edit'),
                'delete_asset_route'=>url('/asset/remove'),
                'listing'=>url('/asset/listing'),
            ]),
        ]);
    }

    public function listing()
    {
        $is_admin =  $this->user->is_admin;
            
        $data = Asset::where('status',1);

        // if($is_admin != 1){
        //     $data = $data->whereNull('is_admin')->orWhere('is_admin', '!=' , 1);
        //     $data = $data->where('role_id','<',$this->user->role_id);
        // }
        
        $data = $data->get();

        return json_encode($data);
    }

    public function addAssetForm()
    {
        $is_admin = session('admin');
        $is_admin = '';
        



        return view('main',[
            'menus'=> json_encode($this->menus),
            'user'=> json_encode($this->user),
            'page_title' => 'Assets Management',
            'page' => 'assetAddform',
            'active_menu'=>9,
            'extra_info'=>json_encode([
                'back_route'=> url('/assets/index'),
                'submit_form' => url('/asset/store'),
                
            ]),
        ]);
    }
}
