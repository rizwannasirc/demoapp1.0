<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\State;
use App\City;

class AddressController extends Controller
{
    public function ajaxGetStates (Request $request)
    {
        $country_id = $request->country_id;

        $Country = Country::find($country_id);

        return json_encode($Country->states);

    }

    public function ajaxGetCities (Request $request)
    {
        $state_id = $request->state_id;

        $state = State::find($state_id);

        return json_encode($state->cities);

    }
}
