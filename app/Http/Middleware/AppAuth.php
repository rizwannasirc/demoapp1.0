<?php

namespace App\Http\Middleware;

use Closure;
use App\App_request;

class AppAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   

        $headers = $request->header();
        
        App_request::create([
            'headers'=> json_encode($headers),
            'params'=> json_encode($request->all()),
            'device'=>$request->header('User-Agent'),
            'ip'=>$request->ip()
        ]);
        
      
        if(isset($headers['appauth']) && $headers['appauth'][0] =='3kkd9i-1kkiod9-88bv823i-62k3kd-9cik2k3')
        {
            
                return $next($request);
        }
        else {

            return redirect('api/unauthorized');
            
        }
      
    }
}
