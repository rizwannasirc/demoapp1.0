<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function Jamatkhanas()
    {
        return $this->hasMany('App\Jamatkhana');
    }

}
