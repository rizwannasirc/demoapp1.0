<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

}
