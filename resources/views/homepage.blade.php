<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="icon" href="{{ asset('images/icon.png') }}" type="image/png" sizes="16x16">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>

        .background {
            background-image: url("{{ asset('images/ismaliBackground2.png') }}");
            min-width: 100vw;
            min-height:100vh;
            background-attachment: fixed;
            background-size:cover;
        }

    </style>
</head>
<body class='background'>
    <v-app id='app'>
        <Main-page></Main-page>
    </v-app>

    <script src="{{asset('js/app.js')}}"></script>
    <script>
        
    </script>
    

</body>
</html>