<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="icon" href="{{ asset('images/icon.png') }}" type="image/png" sizes="16x16">
        <title>Demo App 1.0</title>


    </head>
    <body>
       <v-app id='app'>
       <layout-component
            :menus="{{ $menus }}" :user = "{{ $user }}"
            page='{{ $page }}'  page-title='{{ $page_title }}'
            active-menu='{{ $active_menu??"" }}' extra-info='{{ $extra_info??''  }}'
            logout-link="{{ url('/logout') }}"  loginlink="{{ url('/') }}"
        />
       </v-app>
        <script src="{{asset('js/app.js')}}"></script>
        <script>

        </script>
    </body>
</html>
