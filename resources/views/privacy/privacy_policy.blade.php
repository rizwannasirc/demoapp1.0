<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="icon" href="{{ asset('images/icon.png') }}" type="image/png" sizes="16x16">
        <title>Privacy Policy::Headcount Pro</title>
    </head>
    <body>
        <div style='padding:25px;'>
            <h2 style='color:teal'>Privacy Policy</h2>
            <br />
            <p style='text-align: justify;text-justify: inter-word;font-size:12px;'>
            
                Headcount Pro has developed policies and procedures (the “Privacy Policy”) to protect our website users’ personal information and to ensure that Headcount Pro complies with current applicable privacy legislation.
                Headcount Pro reserves the right to change the Privacy Policy at any time.  As a result, Headcount Pro requests that you, as a website user (“you”), read this section from time to time in order to be informed of possible changes in the Headcount Pro’s Privacy Policy.  If the Privacy Policy changes, and personal information is collected for a purpose that is different from that stated at the time that it was originally collected, the Privacy Policy will be modified and updated as necessary. Changes to the Privacy Policy will take effect on the 31st day from the date the updated Privacy Policy is posted. In addition to modifying the date, Headcount Pro will post all updates to the Privacy Policy on the “Privacy Policy” page of www.headcountpro.com/privacy-policy(the “Website”).
            </p>

            <br />
            <h4 style='color:teal'>TYPE OF INFORMATION COLLECTED</h4>
            <p style='text-align: justify;text-justify: inter-word;font-size:12px;'>
                Headcount Pro may collect the following personal information from you when you create an account in, or submit information to, the Website or App: first name, last name and e-mail address only. You may consent to the use of personal information and you may withdraw consent to the use of this information (excluding such information which may be legally retained indefinitely and such information which is maintained in anonymized aggregate form by Headcount Pro) at any time (subject to reasonable notice and the legal obligations Headcount Pro is subject to).  If no explicit consent is provided, your consent is implied.  You may withdraw consent by contacting Headcount Pro sending your request at 
                <b style='color:teal;text-decoration:underline;'> admin@headcountpro.com </b>
                . Please note that withdrawing consent may result in a reduction in access to the Website and the application.
                Additionally, please note that Headcount Pro may aggregate the data (including personal information) it collects and, by disclosing your personal information on this Website, you hereby agree and consent to Headcount Pro’s collection, use and disclosure of such data in aggregate anonymized form among the jamati institutions (the “Institutions”) and third parties who support the mandate or the Institutions.
            </p>

            <br />
            <h4 style='color:teal'>USE OF PERSONAL INFORMATION</h4>
            <p style='text-align: justify;text-justify: inter-word;font-size:12px;'>
                Headcount Pro and the Institutions may use the personal information collected by them from you for the following reasons:
                <ul style='padding-left:25px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                    <li>To communicate with you and/or deliver information and services to you regarding the work of the Institutions;</li>
                    <li>For Institutional planning purposes including, without limitation, to assist with the creation, review and implementation of programs, services and measures intended to benefit the Shia Imami Ismaili Muslim community in Canada and throughout the world;</li>
                    <li>To conduct statistical analysis necessary to further assist Headcount Pro and the Institutions in fulfilling their mandates;</li>
                    <li>To improve Headcount Pro’s Website features and content;</li>
                    <li>To analyze Website usage;</li>
                    <li>For market research;</li>
                    <li>For the planning of or response to a natural disaster, emergency event or imminent threat to the Shia Imami Ismaili Muslim community in Canada and throughout the world;</li>
                    <li>To send you promotional materials and other information with your consent (as same may be provided herein or otherwise);</li>
                    <li>To respond to your comments or questions; and</li>
                    <li>To provide you with content that is customized to your interests based on the information you have provided to us and your activities on the site.</li>
                </ul>
            </p>

            <br />
            <h4 style='color:teal'>USE OF COOKIES</h4>
            <p style='text-align: justify;text-justify: inter-word;font-size:12px;'>
                When you navigate the Website, small files called ‘cookies’ may be attached to your web browser.  These files identify your web browser and save information such as passwords so that websites can recognize you.  You can set your browser to disable cookies, but some websites may not work properly if you do this.
                <br /><br />Headcount Pro uses the following types of cookies: 
                <ul style='padding-left:25px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                    <li>
                        <h4>Session Cookies:</h4>
                        <p>These are used as an integral part of the identification process for some of Headcount Pro’s online applications. This is for security purposes to determine that you are who you say you are and to provide you with your confidential account information during an online session. No personal information is kept in session cookies and they are stored only in your browser's temporary (cache) memory. When you log out of your session, the cookie is no longer valid and is discarded when you close your browser.</p>
                    </li>
                    
                </ul>
            </p>

            <p style='padding-top:20px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                Headcount Pro may also use Google Analytics.  Google Analytics, a web analytics service provided by Google Inc., helps to measure traffic patterns to, from, and within the Website and uses cookies to collect aggregated information about you.  Information collected by Google Analytics is stored on Google servers in the United States and may be subject to the governing legislation in that country (for example, the USA Patriot Act).  Your IP address is anonymized prior to being stored on Google’s servers in order to help safeguard your privacy. For further information about Google Analytics, please refer to 
                <a>http://www.google.com/intl/en_ALL/policies/privacy/</a>.
            </p>
            <p style='padding-top:20px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                If you wish, you may opt out of being tracked by Google Analytics by disabling or refusing the cookies; by disabling JavaScript within your browser; or by using the https://tools.google.com/dlpage/gaoptout?hl=en. Disabling Google Analytics or JavaScript will still permit you to access comparable information or services from the Website.
            </p>

            <br />
            <h4 style='color:teal'>DISCLOSURE OF PERSONAL INFORMATION</h4>
            <p style='text-align: justify;text-justify: inter-word;font-size:12px;'>
                Under no circumstances will your personal information, which Headcount Pro collects, be communicated or disseminated to third parties, except as provided herein or unless this is necessary to carry out the services you request on the Website.  
            </p>
            <p style='padding-top:20px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                In the normal course of business, Headcount Pro may disclose some personal information to service providers and suppliers.  Headcount Pro currently discloses personal information to the following third parties:
                <ul style='padding-left:25px;text-align: justify;text-justify: inter-word;font-size:12px;' >
                    <li>Google Analytics, a Web analytics service provided by Google Inc. – Used to generate detailed statistics about the Website’s traffic and traffic sources and measure conversions and sales</li>
                </ul>
            </p>
            <p style='padding-top:20px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                Headcount Pro must also disclose personal information when required by law.  For example, if Headcount Pro is presented with a search warrant or other legal order or if personal information is requested from an investigative body in relation to a breach of the law, personal information may be disclosed.
            </p>

            <br />
            <h4 style='color:teal'>PROTECTION OF PRIVACY AND PERSONAL INFORMATION</h4>
            <p style='text-align: justify;text-justify: inter-word;font-size:12px;'>
                Personal information will be processed by persons in charge of the processing for the purposes described above.  In particular, these persons will record, organize, keep, elaborate, modify, select, and use or delete personal data to carry out the operations necessary to carry out the purposes for which you have given your consent.
            </p>
            <p style='padding-top:20px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                In the context of this Website, the processing of personal information will be performed by Headcount Pro and will be carried out using both paper and electronic tools.
            </p>
            <p style='padding-top:20px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                Headcount Pro is committed to ensuring that your personal information is secure and kept confidential.  
            </p>
            <p style='padding-top:20px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                With respect to the processing of personal data, you will be entitled to do the following:
                <ul style='padding-left:25px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                    <li>obtain a confirmation, at your written request, from Headcount Pro as to whether or not personal information relating to you has been collected, and whether such information is being used and/or disclosed to third parties;</li>
                    <li>request the deletion of (i) your individualized personal information which was collected, used or disclosed unlawfully;</li>
                    <li>request updating, rectification or, where applicable, completion of your personal information; and</li>
                    <li>request a statement confirming that the operations requested above were performed.</li>
                </ul>
            </p>
            <p style='padding-top:20px;text-align: justify;text-justify: inter-word;font-size:12px;'>
                Headcount Pro will disclose breaches of your personal information as required by applicable law.
            </p>
            
            <br />
            <h4 style='color:teal'>RETENTION OF INFORMATION</h4>
            <p style='text-align: justify;text-justify: inter-word;font-size:12px;'>
                Headcount Pro will retain personal information as long as it is necessary for the purposes described in this Privacy Policy and as required by law, after which time, the information will be deleted and destroyed.
            </p>

        </div>
        
    </body>
</html>