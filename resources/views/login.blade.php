<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="icon" href="{{ asset('images/icon.png') }}" type="image/png" sizes="16x16">
        <title>Login::Demo App 1.0</title>

        <!-- <meta name="google-signin-scope" content="profile email"> -->
        <meta name="google-signin-client_id" content="1005415548574-ide6u9uj902dngsn8ert5o2i9knkrsg2.apps.googleusercontent.com">
        <script src="https://apis.google.com/js/api:client.js"></script>

    </head>
    <body>
       <v-app id='app'>
        <login-component
            source='{{ url('/validate') }}'
            authchecking-link='{{ url('/Auth2validate') }}'
            servermessages= '{{ json_encode(session("alert")) }}'
        ></login-component>
       </v-app>
        <script src="{{asset('js/app.js')}}"></script>
        {{-- <script src="https://apis.google.com/js/platform.js" async defer></script> --}}
        {{-- <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script> --}}

        <script>



            // function renderButton() {
            //     gapi.signin2.render('my-signin2', {
            //         'scope': 'profile email',
            //         'width': 240,
            //         'height': 50,
            //         'longtitle': true,
            //         'theme': 'dark',
            //         'onsuccess': onSuccess,
            //         'onfailure': onFailure
            //     });
            // }

            // function onSuccess()
            // {


            // }

            // function onFailure()
            // {

            // }

        </script>
    </body>
</html>
