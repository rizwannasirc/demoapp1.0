/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require('vue');
import vuetify from "./designmaterial";
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('login-component', require('./components/LoginComponent.vue').default);
import LoginComponent from "./components/LoginComponent.vue";
import LayoutComponent from "./components/LayoutComponent.vue";
import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyAn7_8HxY57PxKtmckhRUQ3UhVWbKNjcyg',
      libraries: 'places', // This is required if you use the Autocomplete plugin
    },
    // installComponents: true
  });
// import HighchartsVue from 'highcharts-vue';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.use(HighchartsVue);

const app = new Vue({
    el: '#app',
    vuetify,
    components: {
        LoginComponent,
        LayoutComponent,

    }
});
